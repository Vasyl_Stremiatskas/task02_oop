import java.util.*;

    class Refrigerator extends Device {
        private static int number = 0;
        
        Refrigerator(int power) {
            super(power);
            number++;
            String name = "refrigerator" + Integer.toString(number);
            setName(name);
        }
    }