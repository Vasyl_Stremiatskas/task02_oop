import java.util.*;

    class Kettle extends Device {
        private static int number = 0;
        
        Kettle(int power) {
            super(power);
            number++;
            String name = "kettle" + Integer.toString(number);
            setName(name);
        }
    }