import java.util.*;

class Computer extends Device {
        private static int number = 0;
        
        Computer(int power) {
            super(power);
            number++;
            String name = "computer" + Integer.toString(number);
            setName(name);
        }
    }