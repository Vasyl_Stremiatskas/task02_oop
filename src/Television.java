import java.util.*;

    class Television extends Device {
        private static int number = 0;
        
        Television(int power) {
            super(power);
            number++;
            String name = "television" + Integer.toString(number);
            setName(name);
        }
    }
