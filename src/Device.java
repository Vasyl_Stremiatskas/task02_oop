import java.util.*;

    abstract class Device implements Comparable<Device> {
        private String deviceName;
        private int power;
        private boolean connectivity;
        private Vector connectivityTimeList;
        
        Device(int ppower) {
            connectivityTimeList = new Vector();
            power = ppower;
        }
        
        public String getDeviceName() {
            return deviceName;
        }
        
        public void setName(String name) {
            deviceName = name;
        }
        
        public boolean isConnectivity() {
            return connectivity;
        }
        
        protected void connect() {
            if(!connectivity) {
                connectivity = true;
                Date connectTime = new Date();
                connectivityTimeList.add(connectTime);
            }
        }
        
        protected void disconnect() {
            if(connectivity) {
                connectivity = false;
                Date disconnectTime = new Date();
                connectivityTimeList.add(disconnectTime);
            }
        }
        
        protected void connectDisconnect() {
            if(isConnectivity) {
                disconnect();
                System.out.println("Your device is disconnect");
            }
            else {
                connect() {
                System.out.println("Your device is connect");
                }
            }
            
        }
        
        protected void showConnectivityTimeList() {
            int connect = 1;
            Enumeration vEnum = connectivityTimeList.elements();
            while(vEnum.hasMoreElements()) {
                if(connect%2 == 1) {
                    System.out.println(" Disconnect: " + vEnum.nextElement());
                    connect++;
                }
                else {
                    System.out.println(" Connect: " + vEnum.nextElement());
                    connect++;
                }
            }
        }
        
        protected double getLastUsedPower() {
            double usedPower = -1;
            
            if(connectivity) {
                Date currentTime = new Date();
                Date lastConnect = (Date)connectivityTimeList.lastElement();
                double currentSeconds = currentTime.getTime()/36000;
                double lastSeconds = lastConnect.getTime()/36000;
                usedPower = (currentSeconds - lastSeconds) * (double)power; 
            }
            
            if(!connectivity) {
                Date lastConnectTime;
                Date lastDisconnectTime;
                lastConnectTime = (Date)connectivityTimeList.get(connectivityTimeList.size()-1);
                lastDisconnectTime = (Date)connectivityTimeList.lastElement();
                double connectSeconds = lastConnectTime.getTime()/36000;
                double disconnectSeconds = lastDisconnectTime.getTime()/36000;
                usedPower = (disconnectSeconds - connectSeconds) * (double)power; 
            }
            
            return usedPower;
        }
        
        public int getPower() {
            return power;
        }
        
        public String getInfo() {
            String isConnect;
            if(isConnectivity()) {
                isConnect = "connect";
            }
            else {
                isConnect = "disconnect";
            }
            return ("Device name: " + deviceName + "Connectivity: " + isConnect + "Power: " + power);
        }
        
        @Override
        public int compareTo(Device device) {
            return (this.getPower() - device.getPower());
        }
    }
    
    