class HomeManager {
        private Vector deviceList;
        
        HomeManager() {
            deviceList = new Vector();
        }
        
        void addDevice(Device d) {
            deviceList.add(d);
        }
        
        void addDevice() {
            int type;
            int power;
            Scanner s = new Scanner(System.in);
            Device newDevice = null;
            
            try {
                System.out.println("Enter a type of device: ");
                System.out.println("1 - Computer ");
                System.out.println("2 - Regrigerator");
                System.out.println("3 - Microwave");
                System.out.println("4 - Television");
                System.out.println("5 - Kettle");
                System.out.print("Your answer: ");
                type = s.nextInt();
                
                System.out.print("Enter the power: ");
                power = s.nextInt();
                
                switch(type) {
                    case(1): newDevice = new Computer(power);
                             break;
                    case(2): newDevice = new Refrigerator(power);
                             break;
                    case(3): newDevice = new Microwave(power);
                             break;
                    case(4): newDevice = new Television(power);
                             break;
                    case(5): newDevice = new Kettle(power);
                             break;
                    default: addDevice();
                             break;
                }
                
                addDevice(newDevice);
            }
            
            catch(Exception e) {
                System.out.println(e.toString());
            }
        }
        
        void deleteDeviceByName(String deviceName) {
            Device device;
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                if(deviceName == device.getDeviceName()) {
                    deviceList.removeElement(device);
                }
            }
        }
        
        int getUsedPower() {
            Device device;
            int usedPower = 0;
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                if(device.isConnectivity()) {
                    usedPower += device.getPower();
                }
            }
            
            return usedPower;
        }
        
        void sortDevices() {
           Collections.sort(deviceList);
        }
        
        void showAllDevices() {
            Device device;
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                System.out.println(device.getDeviceName());
            }
        }
        
        void showConnectedDevices() {
            Device device;
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                if(device.isConnectivity()) {
                    System.out.println(device.getDeviceName());
                }
            }
        }
        
        void showInfoByName(String name) {
            Device device;
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                if(device.getDeviceName() == name) {
                    System.out.println(device.getInfo());
                }
            }
            
        }
        
        String getNameByInfo() {
            int power = 0;
            boolean isConnect = false;
            int connectivity;
            String name = "none";
            Scanner scan = new Scanner(System.in);
            Device device;
            
            try {
                System.out.print("Write a power of device: ");
                power = scan.nextInt();
                System.out.print("Is device connect to the electricity (write 1 if yes, and 0 if no): ");
                connectivity = scan.nextInt();
                if(connectivity == 1) {
                    isConnect = true;
                }
                else {
                    isConnect = false;
                }
            }
            catch(Exception e) {
                System.out.println(e.toString());
                getNameByInfo();
            }
            
            for(int i = 0; i < deviceList.size(); i++) {
                device = (Device)deviceList.get(i);
                if(device.isConnectivity() == isConnect && device.getPower() == power) {
                    name = device.getDeviceName();
                }
            }
            return name;
        }
        
        void UI() {
            int choose = -1;
            Scanner s = new Scanner(System.in);
            String info = null;
            try {
                System.out.println("Hi, what do you want to do?");
                System.out.println("0 - nothing;");
                System.out.println("1 - add new device;");
                System.out.println("2 - remove device by name;");
                System.out.println("3 - connect device;");
                System.out.println("4 - disconnect device;");
                System.out.println("4 - get name of connect devices;");
                System.out.println("5 - get name of all devices;");
                System.out.println("6 - get info about using electricity at the moment;");
                System.out.println("7 - get name by info;");
                System.out.println("8 - get info by name;");
                System.out.print("Your answer: ");
                choose = s.nextInt();
                
                switch(choose) {
                    case(1):addDevice();
                            UI();
                            break;
                    case(2):info = s.nextLine; 
                            deleteDeviceByName(info);
                            UI();
                            break;
                    case(3): 
                    
                }
              
            }  
            catch(Exception e) {
                System.out.println(e.toString());
            }
        }
    }