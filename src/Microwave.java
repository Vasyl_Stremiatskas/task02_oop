import java.util.*;

    class Microwave extends Device {
        private static int number = 0;
        
        Microwave(int power) {
            super(power);
            number++;
            String name = "microwave" + Integer.toString(number);
            setName(name);
        }
    }